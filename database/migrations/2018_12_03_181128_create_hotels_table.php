<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('lugar_id')->unsigned();
            $table->smallInteger('wifi')->default(0);
            $table->smallInteger('estacionamiento')->default(0);
            $table->smallInteger('restaurante')->default(0);
            $table->smallInteger('lavanderia')->default(0);
            $table->decimal('costo_dia', 10,2)->default(0.00);
            $table->decimal('rating', 3,1)->default(0.0);
            

            $table->foreign('lugar_id')->references('id')->on('lugars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
