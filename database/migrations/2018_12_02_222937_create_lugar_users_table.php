<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLugarUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugar_users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            // 0 = NO MARCADO 1 = MARCADO
            $table->datetime("marcado")->nullable();
            $table->datetime("visitado")->nullable();
            $table->datetime('favorito')->nullable();

            $table->integer('user_id')->unsigned();
            $table->integer('lugar_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lugar_id')->references('id')->on('lugars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lugar_users');
    }
}
