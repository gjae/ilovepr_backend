<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLugarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lugars', function (Blueprint $table) {
            $table->string('phone')->default('00000000');
            $table->string('email')->default("dontHas@mail.com");
            $table->string("fb_link")->nullable();
            $table->string("twitter_link")->nullable();
            $table->string("ig_link")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lugars', function (Blueprint $table) {
            $table->dropColumn(['phone', 'email', 'fb_link', 'twitter_link', 'ig_link']);
        });
    }
}
