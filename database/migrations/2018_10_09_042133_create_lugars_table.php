
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLugarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nombre', 200);
            $table->text('longitud')->nullable();
            $table->text('latitud')->nullable();
            $table->text('direccion')->nullable();
            $table->text('resena')->nullable();
            $table->integer('ciudad_id')->unsigned();
            $table->enum('tipo', [
                'HOTEL', 
                'RESTAURANTE', 
                'CAFETERIA', 
                'HISTORICO', 
                'CLUB_NOCTURNO', 
                'SUPER_MERCADO', 
                'MALL',
                'P/D' , // POR DEFINIR
            ])->default('P/D');

            $table->time('hora_apertura')->nullable();
            $table->time('hora_cierre')->nullable();
            $table->date('fecha_inauguracion')->nullable();
            $table->enum('estado_actual', [
                'ABIERTO', 
                'CERRADO', 
                'CLAUSURADO', 
                'ABANDONADO',
                'P/D' // POR DEFINIR
            ])->default('P/D');


            $table->foreign('ciudad_id')->references('id')->on('ciudads');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lugars');
    }
}
