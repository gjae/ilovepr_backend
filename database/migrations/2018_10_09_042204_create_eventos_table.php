<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nombre', 200);
            $table->integer('ciudad_id')->unsigned();
            $table->text('descripcion')->nullable();
            $table->enum('tipo', [
                'PUBLICO', 'PRIVADO'
            ]);
            $table->decimal('costo_entrada', 12, 2)->default(0.00);
            $table->datetime('fecha_hora');
            $table->enum('tipo_espacio', [
                'AIRE_LIBRE', 'CERRADO'
            ])->default('CERRADO');

            $table->integer('categoria_id')->unsigned()->default(0);
            $table->string('imagen_promocional')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
