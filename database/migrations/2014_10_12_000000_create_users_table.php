<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('device_id')->nullable();
            $table->text('fb_token')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();

            // ESTA COLUMNA DENOTA EL TIPO DE USUARIO QUE SE LOGEA
            // LOS ADMINS SON LOS QUE MANEJAN EL PANEL ADMINISTRATIVO
            // LOS USUARIOS SON AQUELLOS QUE INTERACTUAN DESDE LA APP
            $table->enum('rol', [ 'ADMIN', 'USER' ])->default('USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
