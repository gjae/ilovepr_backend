<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('pais_id')->unsigned()->default(0);
            $table->integer('pais_location_id')->unsigned()->default(0);
            $table->enum('social_network', ['FACEBOOK', 'GOOGLE', 'TWITTER', 'INSTAGRAM', 'LINKEDIN'])
            ->default('FACEBOOK');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['pais_id', 'pais_location_id', 'social_network']);
        });
    }
}
