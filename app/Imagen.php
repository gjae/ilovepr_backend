<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imagen extends Model
{
    use SoftDeletes;
    protected $table = 'imagens';
    protected $fillable = [
    	'nombre', 'mime', 'size', 'original_extension', 'nombre_original', 'ubicacion'
    ];

    public function eventos(){
    	return $this->hasMany('App\EventoImagen');
    }
}
