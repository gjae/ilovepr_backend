<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model
{
    use SoftDeletes;
    protected $table = 'eventos';
    protected $fillable = [

    	'nombre',
    	'ciudad_id',
    	'descripcion',
    	'tipo',
    	'costo_entrada',
    	'fecha_hora',
    	'tipo_espacio',
        'categoria_id',
        'organizador_id'
    ];

    protected $casts = [
    	'fecha_hora' => 'datetime'
    ];

    protected $dates = ['deleted_at', 'fecha_hora'];



    public function categoria(){
    	return $this->belongsTo('App\Categoria');
    }

    public function imagenes(){
        return $this->hasMany('App\EventoImagen');
    }

    public function organizador(){
        return $this->belongsTo('App\Organizador','organizador_id', 'id');
    }

    public function marcado(){
        return $this->hasOne('App\EventoUser', 'evento_id', 'id');
    }
}
