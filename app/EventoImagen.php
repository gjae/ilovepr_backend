<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoImagen extends Model
{
    use SoftDeletes;
    protected $table= 'evento_imagens';
    protected $fillable = [
    	'evento_id', 'imagen_id'
    ];


    public function imagen(){
    	return $this->belongsTo('App\Imagen');
    }

    public function evento(){
    	return $this->belongsTo('App\Evento');
    }
}
