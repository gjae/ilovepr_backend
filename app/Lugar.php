<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lugar extends Model
{
    use SoftDeletes;
    protected $table = 'lugars';
    protected $fillable = [

    	'nombre', 'longitud', 'latitud', 'resena', 'ciudad_id', 'tipo', 'hora_apertura', 'hora_cierre', 
        'fecha_inauguracion', 'estado_actual', 'direccion', 'categoria_id',
        'phone', 'email', 'fb_link', 'twitter_link', 'ig_link'
    ];

    protected $casts = [
    	'fecha_inauguracion' => 'date'
    ];


    public function ciudad(){
    	return $this->belongsTo('App\Ciudad');
    }

    public function imagenes(){
    	return $this->hasMany('App\LugarImagen');
    }

    public function comentarios(){
        return $this->hasMany('App\ComentarioLugar');
    }

    public function marcado(){
        return $this->belongsTo('App\LugarUser', 'id', 'lugar_id');
    }

    public function hotel(){
        return $this->hasOne('App\Hotel', 'lugar_id', 'id');
    }
}
