<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LugarImagen extends Model
{
    use SoftDeletes;
    protected $table = 'lugar_imagens';
    protected $fillable = [
    	'lugar_id', 'imagen_id'
    ];

    public function imagen(){
    	return $this->belongsTo('App\Imagen');
    }

}
