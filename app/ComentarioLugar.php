<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioLugar extends Model
{
    protected $table = 'comentario_lugars';
    protected $fillable = [
        'user_id', 'comentario', 'rating', "lugar_id"
    ];

    public function usuario(){
        return $this->belongsTo('App\User', "user_id");
    }
}
