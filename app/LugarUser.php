<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LugarUser extends Model
{
    use SoftDeletes;
    protected $table = 'lugar_users';
    protected $fillable = [
        'marcado', 'visitado', 'favorito', 'user_id', 'lugar_id'
    ];

    public function usuario(){
        return $this->belognsTo('App\User', 'user_id', 'id');
    }

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id');
    }
}
