<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  Illuminate\Database\Eloquent\SoftDeletes;
class Organizador extends Model
{
    use SoftDeletes;
    protected $table = 'organizadors';
    protected $fillable = [
        'nombre', 'email', 'website', 'twitter_link', 'instagram_link', 'identificacion',
        'twitter_link', 'facebook_link', 'phone', 'direccion', 'img'
    ];

    public function eventos(){
        return $this->hasMany('App\Evento');
    }
}
