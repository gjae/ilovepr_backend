<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoUser extends Model
{
    use SoftDeletes;
    protected $table = 'evento_users';
    protected $fillable = [
        'user_id', 'marcado', 'quiero_asistir', 'evento_id', 'compartido'
    ];

    protected $casts = [
        'marcado' => 'datetime',
        'quiero_asistir' => "datetime",
        'compartido' => "datetime"
    ];

    protected $date = [ 'deleted_at', 'marcado', 'quiero_asistir', 'compartido' ];

    public function evento(){
        return $this->belongsTo('App\Evento');
    }

    
    public function usuario(){
        return $this->belongsTo('App\User');
    }
}
