<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;
    protected $table = 'hotels';
    protected $fillable = [
        'wifi', 'estacionamiento', 'restaurante', 'lavanderia', 'costo_dia', 'rating', 'lugar_id'
    ];

    public function lugar(){
        return $this->belongsTo('App\Lugar', 'lugar_id', 'id');
    }

}
