<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

use Auth;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('generics.categorias', ['user' => Auth::user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response([
            'error' => false,
            'html' => \View::make('partials.categoria', ['categoria' => false, 'url' => 'dashboard/categorias'])->render(),
            
        ], 200)->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = new Categoria($request->all());
        $categoria->save();

        return redirect()->to( url("dashboard/categorias") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response([
            'error' => false,
            'html' => \View::make('partials.categoria', ['categoria' => Categoria::find($id),  "url" => "dashboard/categorias/$id" ])->render(),

        ], 200)->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);

        $categoria->nombre = $request->nombre;

        $categoria->save();

        return redirect()->to( url("dashboard/categorias") );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);

        if( $categoria )
            $categoria->delete();

        return response([
            'error' => false,
            'message' => 'La categoria ha sido eliminada de manera correcta'
        ], 200)->header('Content-Type', 'application/json');
    }
}
