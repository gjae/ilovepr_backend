<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evento;
use App\EventoUser;
use Carbon\Carbon;
use Auth;

class EventsController extends Controller
{
    public function get(Request $request){
        $user = $this->guard()->user();

        $events = ( $request->has('ciudad') && $request->ciudad != '' && $request->ciudad != "todas"  && $request->ciudad != null ) 
        ? Evento::whereCiudadId($request->ciudad) 
        : new Evento;

        $events = $events->with(['imagenes.imagen', 'categoria', 'organizador'])
        ->with(['marcado' => function($query) use(&$user, &$request){
            $query->whereUserId($user->id)->where(function($query) use(&$request){

                    return $query->whereNull('marcado')->where(function($query){    
                        return $query->whereNotNull('quiero_asistir')->orWhereNotNull('compartido');
                    })->orWhereNull('quiero_asistir')->where(function($query){
                        return $query->whereNotNull('marcado')->orWhereNotNull('compartido');
                    })->orWhereNull('compartido')->where(function($query){
                        return $query->whereNotNull('marcado')->orWhereNotNull('quiero_asistir');
                    });  
            });
        }])
        ->paginate(4);



        $response = [
            'events' => $events->items(),
            'total_pages' => $events->lastPage()
        ];

        return response($response, 202)->header('Content-Type', 'application/json');
    }


    public function save(Request $request){
        $user = $this->guard()->user();
        return call_user_func_array([$this, $request->to.ucfirst( strtolower($request->is) )], [ $request ] );
    }

    private function markInteres($request){
        $eu = $this->findOrCreate($request);
        if( is_null($eu->marcado) ){
            $eu->marcado = Carbon::now();
            $eu->save();
        }
        return $this->responseWithSuccess();
    }

    private function markAsistencia($request){
        $eu = $this->findOrCreate($request);
        if( is_null( $eu->quiero_asistir ) ) $eu->quiero_asistir = Carbon::now();
        $eu->save();
        return $this->responseWithSuccess();
    }

    private function dismarkAsistencia($request){
        $eu = $this->findOrCreate( $request );
        $eu->quiero_asistir = null;
        $eu->save();
        return $this->responseWithSuccess();
    }

    private function dismarkInteres($request){
        $eu = $this->findOrCreate($request);
        $eu->marcado = null;
        $eu->save();
        return $this->responseWithSuccess();
    }

    private function responseWithSuccess(){
        return response(['message' => "Success"], 202)->header('Content-Type', 'application/json');
    }

    private function findOrCreate($request) : EventoUser {
        $user = $this->guard()->user();
        return EventoUser::whereUserId($user->id)->whereEventoId($request->evento_id)->firstOrCreate([
            'evento_id' => $request->evento_id,
            'user_id' => $user->id
        ]);
    }

    private function guard(){
        return Auth::guard('api');
    }
}
