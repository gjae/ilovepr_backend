<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Lugar;
use App\ComentarioLugar;
use App\LugarUser;
use Carbon\Carbon;
use App\ComentarioLugar as CL;
use App\Ciudad;
class PlacesController extends Controller
{
    public function places(Request $request){
        $lugares = Lugar::whereDoesntHave('hotel');
        if($request->has('tipo') && $request->tipo != "" && $request->tipo !=  "Lugares")
            $lugares = $lugares->whereTipo($request->tipo);

        $user = $this->guard()->user();
        $lugares =$lugares->with(['comentarios.usuario', 'imagenes.imagen'])
        ->with(['marcado' => function($query) use (&$user){
            $query->whereUserId($user->id)->whereNotNull('favorito');
        }]);

        if( $request->has('ciudad') && $request->ciudad != "todas" )
            $lugares = $lugares->whereCiudadId($request->ciudad);

        $response = [
            'lugares' =>  $lugares->get(),
        ];

        return response($response, 202)->header('Content-Type', 'application/json');
    }

    public function ciudades(Request $request){
        $ciudades = Ciudad::select(['id as value', 'nombre as label', 'id as key'])->addSelect(\DB::raw("'#ef5350' as color"))->get()->toArray();

        return response(['ciudades' => $ciudades], 202)->header('Content-Type', 'application/json');
    }

    public function getAll(Request $request){
        $places = Lugar::whereNotNull('longitud')->whereNotNull('latitud');
        $position = explode(",", $request->position);
        // 0 = lugars 1 = request
        if( $request->has('position') ){
            $places = $places->selectRaw("
                acos( 
                    cos(radians( lugars.latitud ))
                * cos(radians( ? ))
                * cos(radians( lugars.longitud ) - radians( ? ))
                + sin(radians( lugars.latitud )) 
                * sin(radians( ? ))
                ) / 1000 + 2.6  as distancia , lugars.*
            ")->setBindings([ $position[1], $position[0], $position[1] ])->with(['imagenes.imagen', 'comentarios.usuario']);
        }

        return response(['lugares' => $places->get()], 202)->header('Content-Type', 'application/json');
    }

    public function comment(Request $request){
        $user = $this->guard()->user();
        $comment =  CL::whereUserId($user->id)->firstOrCreate([
            'user_id' => $user->id,
            'lugar_id' => $request->lugar_id,
            'comentario' => $request->comentario
        ]);

        return response(['message' => 'Success', "comment" => $comment, 'usuario' => $comment->usuario ], 202)->header('Content-Type', 'application/json');
    }

    public function save(Request $request){
        $user = $this->guard()->user();
        return call_user_func_array([$this, $request->to.ucfirst( strtolower($request->is) )], [ $request, $user ] );
    }

    private function markFavorito($request, $user){
        $lu = $this->lugarUsuario($request->lugar_id, $user->id);
        $lu->favorito = Carbon::now()->format('Y-m-d H:i:s');
        $lu->save();
        return response(['message' => "Success"], 202)->header('Content-Type', 'application/json');
    }

    private function dismarkFavorito($request, $user){
        $lu = $this->lugarUsuario($request->lugar_id, $user->id);
        $lu->favorito = null;
        $lu->save();
        return response(['message' => "Success"], 202)->header('Content-Type', 'application/json');
    }

    /**
     * BUSCA O CREA EL PRIMER REGISTRO QUE CONCUERDE CON LUGAR_ID O USER_ID PARE RETORNARLO Y HACER LA OPERACION
     * CORRESPONDIENTE A LA SOLICITUD HTTP
     * @param [INTEGER] $lugar_id
     * @param [INTEGER] $user_id
     * @return App\LugarUser
     */
    private function lugarUsuario($lugar_id, $user_id) : LugarUser{
        $lu = LugarUser::whereUserId($user_id)->whereLugarId($lugar_id)->firstOrCreate([
            "lugar_id" => $lugar_id,
            "user_id" => $user_id
        ]);
        return $lu;
    }

    private function guard(){
        return Auth::guard('api');
    }

}
