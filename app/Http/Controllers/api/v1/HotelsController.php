<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lugar;
use App\Hotel;

use Auth;
class HotelsController extends Controller
{
    public function get(Request $request){

        $lugares = Lugar::whereHas('hotel')
        ->with('hotel')
        ->with(['comentarios.usuario', 'imagenes.imagen'])
        ->get()
        ->toArray();
        
        $response = [
            "hotels" => $lugares
        ];

        return response($response)->header('Content-Type', 'application/json');
    }
}
