<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class Authentication extends Controller
{
    
    /**
     * Crea el login con el token bearer del usuario y lo retorna, si no retorna un error 401
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request){
        return $this->register($request);
        
        return response(['error' => 'Unauthorized'], 401)->header('Content-Type', 'application/json');
    }

    public function me(){
        return response(
            $this->guard()->user()->toArray(),
            202
        )->header('Content-Type', 'application/json');
    }

    public function logout(){
        $this->guard()->logout();
        return response(['message' => 'Success logout'], 202)->header('Content-Type', 'application/json');
    }

    /**
     * Verifica que el usuario se encuentre en la BD
     * Si el usuario no esta registrado en la base de datos
     * entonces lo registra y crea una nueva session
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response 
     */
    public function register($request){

        $user = User::whereEmail($request->email)->where('fb_token', $request->fb_token)->first();
        if(!$user) $user = User::create($request->all());
        
        $token = $this->guard()->login($user);

        return $this->responseWithToken($token);
    }



    /**
     * @method responseWithToken
     */
    private function responseWithToken($token){
        return response([
            'token' => $token,
            'type' => 'bearer',
            'expire_in' => $this->guard()->factory()->getTTL() * 60
        ], 202)->header('Content-Type', 'application/json');
    }

    /**
     * RETORNA EL OBJETO AUTH CON EL GUARD API
     *
     * @return Auth::guard
     */
    private function guard(){
        return Auth::guard('api');
    }
}
