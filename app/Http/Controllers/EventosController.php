<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\Image;
use App\EventoImagen as EI;
use Storage;
use App\Imagen;

use Carbon\Carbon;
use Auth;
class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('generics.eventos', [ 'user' => Auth::user() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.evento', [ 'user' => Auth::user(), 'evento' => false, 'url' => "dashboard/eventos" ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $images = $request->file('images');
     
        $dataRequest = $request->all();
        $dataRequest['fecha'] = str_replace('/', '-', $dataRequest['fecha']);
        $dataRequest['fecha_hora'] = Carbon::parse($dataRequest['fecha'].' '.$dataRequest['hora'])->format('Y-m-d H:i:s');

        $e = Evento::create($dataRequest);
       foreach ($images as $key => $imagen) {
            $path = $imagen->store('images', 'events');

            $imagen = Imagen::create([
                'nombre' => explode('/', $path)[1],
                'mime' => $imagen->getClientOriginalExtension(),
                'size' => $imagen->getSize(),
                'original_extension' => $imagen->getClientOriginalExtension(),
                'nombre_original' => $imagen->getClientOriginalName(),
                'ubicacion' => 'uploads/events/'.$path
            ]);

            EI::create([
                'evento_id' => $e->id,
                'imagen_id' => $imagen->id
            ]);
        }


        return redirect()
        ->to( url("dashboard/eventos") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.evento', [
            'evento' => Evento::find($id),
            'user' => Auth::user(),
            "url" => "dashboard/eventos/$id"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evento = Evento::find($id);
        $dataRequest = $request->except(['_token', '_method', 'DataTables_Table_0_length', 'images']);
        $dataRequest['fecha_hora'] = Carbon::parse( str_replace('/', "-", $dataRequest['fecha']).' '.$dataRequest['hora'] )->format('Y-m-d H:i:s');
         unset( $dataRequest["fecha"] );
         unset( $dataRequest["hora"] );
         $evento->fill($dataRequest);

       foreach ($request->images as $key => $imagen) {
            $path = $imagen->store('images', 'events');

            $imagen = Imagen::create([
                'nombre' => explode('/', $path)[1],
                'mime' => $imagen->getClientOriginalExtension(),
                'size' => $imagen->getSize(),
                'original_extension' => $imagen->getClientOriginalExtension(),
                'nombre_original' => $imagen->getClientOriginalName(),
                'ubicacion' => 'uploads/events/'.$path
            ]);

            EI::create([
                'evento_id' => $evento->id,
                'imagen_id' => $imagen->id
            ]);
        }

        $evento->save();

        return redirect()
        ->to( url("dashboard/eventos") );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e = Evento::find($id);
        if( $e )
            $e->delete();

        return response([
            'error' => false,
            'message' => 'El evento fue eliminado correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }


    public function imagen($img_id){
        $ei = EI::find($img_id);
        $img = Imagen::find( $ei->imagen_id );
        $img->delete();
        $ei->delete();

        return response([
            'error' => false,
            'message' => 'Imagen quitada correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }
}
