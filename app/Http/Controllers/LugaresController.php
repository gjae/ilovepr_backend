<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lugar;
use App\Imagen;
use App\LugarImagen as LI;
use Carbon\Carbon;

use Auth;

class LugaresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'generics.lugares', ['user' => Auth::user(), 'persona' => !is_null( Auth::user()->persona )] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.lugar', [
            'lugar' => false,
            'url' => "dashboard/lugares" ,
            'user' => Auth::user(), 
            'persona' => !is_null( Auth::user()->persona )
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imagenes = $request->file('imagenes');
        $dataRequest = $request->except(['_token', '_method', 'imagenes']);
        $dataRequest["hora_apertura"] = Carbon::parse($dataRequest['hora_apertura'])->format('H:i:s');
        $dataRequest["hora_cierre"] = Carbon::parse($dataRequest['hora_cierre'])->format('H:i:s');
        $lugar = Lugar::create( 
            $dataRequest
        );


       foreach ($imagenes as $key => $imagen) {
            $path = $imagen->store('images', 'lugares');

            $imagen = Imagen::create([
                'nombre' => explode('/', $path)[1],
                'mime' => $imagen->getClientOriginalExtension(),
                'size' => $imagen->getSize(),
                'original_extension' => $imagen->getClientOriginalExtension(),
                'nombre_original' => $imagen->getClientOriginalName(),
                'ubicacion' => 'uploads/lugares/'.$path
            ]);

            LI::create([
                'lugar_id' => $lugar->id,
                'imagen_id' => $imagen->id
            ]);
        }

        return redirect()
        ->to( url("dashboard/lugares") );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lugar = Lugar::find($id);

        return view('pages.lugar', [
            'user' => Auth::user(),
            'lugar' => $lugar,
            "url" => "dashboard/lugares/$id",
            "persona" => !is_null( Auth::user()->id )
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lugar = Lugar::find($id);

        $dataRequest = $request->except(['_token', '_method', 'DataTables_Table_0_length', 'imagenes']);
        $dataRequest['hora_apertura'] = Carbon::parse( $dataRequest['hora_apertura'] )->format('H:i:s');
        $dataRequest['hora_cierre'] = Carbon::parse( $dataRequest['hora_cierre'] )->format('H:i:s');
         $lugar->fill($dataRequest);

       foreach ($request->imagenes as $key => $imagen) {
            $path = $imagen->store('images', 'lugares');

            $imagen = Imagen::create([
                'nombre' => explode('/', $path)[1],
                'mime' => $imagen->getClientOriginalExtension(),
                'size' => $imagen->getSize(),
                'original_extension' => $imagen->getClientOriginalExtension(),
                'nombre_original' => $imagen->getClientOriginalName(),
                'ubicacion' => 'uploads/lugares/'.$path
            ]);

            LI::create([
                'lugar_id' => $lugar->id,
                'imagen_id' => $imagen->id
            ]);
        }

        $lugar->save();

        return redirect()
        ->to( url("dashboard/lugares") );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lugar = Lugar::find($id);
        if( $lugar )
            $lugar->delete();

        return response([
            'error' => false,
            'message' => 'El sitio seleccionado fue eliminado correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }


    public function imagen($img_id){
        $ei = LI::find($img_id);
        $img = Imagen::find( $ei->imagen_id );
        $img->delete();
        $ei->delete();

        return response([
            'error' => false,
            'message' => 'Imagen quitada correctamente'
        ], 200)->header('Content-Type', 'application/json');
    }
}
