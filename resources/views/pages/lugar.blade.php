@extends('layouts.dashboard_app_control')
@section('title', 'Registro de lugares')
@section('title_for_wrapper', 'Administracion de lugares')
@section('panel_header', 'Crear un lugar')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}">
   <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

 <style>
 	
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
        font-family: Roboto;
      }
 </style>
@endsection

@section('dash_content')
	<div class="container">
		<form action="{{ url($url) }}" method="POST" enctype="multipart/form-data">
			@csrf
			@if (!is_bool($lugar))
				@method('PUT')
			@endif
			<div class="form-row">
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Nombre del lugar</label>
					<input type="text" value="{{ is_bool($lugar) ? ''  : $lugar->nombre }}" class="form-control" name="nombre" required="">
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Ciudad</label>
					<select name="ciudad_id" id="ciudad_id" required data-select="Seleccione uno" class="select2 form-control">
						@foreach (App\Ciudad::all() as $ciudad)
							<option {{ !is_bool($lugar) && $lugar->ciudad_id == $ciudad->id ? 'selected'  : '' }}  value="{{ $ciudad->id }}">
								{{ $ciudad->nombre }}
							</option>
						@endforeach
					</select>	
				</div>
				<div class="col-sm-5 col-lg-5 col-md-5">
					@include('partials.autocomplete')
				</div>
			</div>

			<div class="form-row">
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Tipo</label>
					<select name="tipo" id="tipo" class="form-control select2" required="" data-placeholder="Seleccione uno">
						<option value=""></option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'HOTEL' ? 'selected'  : '' }} value="HOTEL">Hotel</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'RESTAURANT' ? 'selected'  : '' }} value="RESTAURANT">
							Restaurant
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'CAFETERIA' ? 'selected'  : '' }} value="CAFETERIA">
							Cafeteria
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'HISTORICO' ? 'selected'  : '' }} value="HISTORICO">
							Lugar Historico
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'CLUB_NOCTURNO' ? 'selected'  : '' }} value="CLUB_NOCTURNO">
							Club Nocturno
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'SUPER_MERCADO' ? 'selected'  : '' }} value="SUPER_MERCADO">
							Super Mercado
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'MALL' ? 'selected'  : '' }} value="MALL">
							Centro Comercial / Mall
						</option>
						<option  {{ !is_bool($lugar) && $lugar->tipo == 'P/D' ? 'selected'  : '' }} value="P/D">
							Por Definir
						</option>
					</select>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Estado actual del sitio</label>
					<select name="estado_actual" id="estado_actual" class="form-control select2" required="" data-placeholder="Seleccione uno">
						<option value=""></option>
						<option {{ !is_bool($lugar) && $lugar->estado_actual == 'ABIERTO' ? 'selected'  : '' }} value="ABIERTO">
							Abierto al publico
						</option>
						<option {{ !is_bool($lugar) && $lugar->estado_actual == 'CERRADO' ? 'selected'  : '' }} value="CERRADO">
							Cerrado indefinidamente
						</option>
						<option {{ !is_bool($lugar) && $lugar->estado_actual == 'CLAUSURADO' ? 'selected'  : '' }} value="CLAUSURADO">
							Clausurado
						</option>
						<option {{ !is_bool($lugar) && $lugar->estado_actual == 'ABANDONADO' ? 'selected'  : '' }} value="ABANDONADO">
							Sitio abandonado
						</option>
						<option {{ !is_bool($lugar) && $lugar->estado_actual == 'P/D' ? 'selected'  : '' }} value="P/D">
							Por definir
						</option>
					</select>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Fecha de inauguración</label>	                
					<div class="input-group">
	                  <div class="input-group-addon">
	                    <i class="fa fa-calendar"></i>
	                  </div>
	                  <input value="{{ is_bool($lugar) ? '' : $lugar->fecha_inauguracion->format('Y-m-d') }}" type="date" id="fecha" name="fecha_inauguracion" class="form-control" >
	                </div>
				</div>
			</div>
			<div class="form-row">
				
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Categoria</label>
					<select name="categoria_id" id="categoria_id" class="form-control select2" data-placeholder="Seleccione unu">
						@foreach (App\Categoria::all() as $categoria)
							<option value="{{ $categoria->id }}">
								{{ $categoria->nombre }}
							</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Hora de apertura</label>
	                  <div class="input-group">
	                    <input value="{{ is_bool($lugar) ? ''  : \Carbon\Carbon::parse($lugar->hora_apertura)->format("h:i:s A") }}" type="text" name="hora_apertura" class="form-control timepicker">

	                    <div class="input-group-addon">
	                      <i class="fa fa-clock-o"></i>
	                    </div>
	                  </div>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Hora de cierre</label>
	                  <div class="input-group">
	                    <input  value="{{ is_bool($lugar) ? ''  : \Carbon\Carbon::parse($lugar->hora_cierre)->format("h:i:s A") }}" type="text" name="hora_cierre" class="form-control timepicker">

	                    <div class="input-group-addon">
	                      <i class="fa fa-clock-o"></i>
	                    </div>
	                  </div>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Imagenes</label>
					<input type="file" multiple class="form-control" name="imagenes[]" id="imagenes">
				</div>
			</div>
			<div class="form-row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<label for="">Reseña / descripción del lugar</label>
					<textarea name="resena" id="resena" cols="30" rows="3" class="form-control" required="">{{ is_bool($lugar) ? ''  : $lugar->resena }}</textarea>
				</div>
			</div>
			<br>
			<div class="form-row">
				<div class="col-sm-12 col-md-12 col-lg-12">
				<label for="">Imagenes promocionales</label>
				@if (!is_bool( $lugar ))
					<table class="table table-bordered table-striped datatables">
						<thead>
							<tr>
								<th>Nombre original</th>
								<th>Tamaño</th>
								<th>Extension</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($lugar->imagenes as $imagen)
								<tr>
									<td>{{ $imagen->imagen->nombre_original }}</td>
									<td>{{ $imagen->imagen->size }}</td>
									<td>{{ $imagen->imagen->original_extension }}</td>
									<td>
										<a class="btn btn-small btn-success imagen" imagen="{{ url($imagen->imagen->ubicacion) }}" img-name="{{ $imagen->imagen->nombre_original }}">
											<i class="fa fa-eye"></i>
										</a>
										<a class="btn btn-small btn-danger delete" type="dashboard/lugares/imagenes" data-delete="{{ $imagen->id }}">
											<i class="fa fa-remove"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@endif
				</div>
			</div>
			<br>
			<br>
			<div class="form-row">
				<div class="col-sm-12 col-lg-12 col-md-12">
					<div class="divider"></div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<button class="btn btn-success btn-large btn-block" type="submit">Guardar</button>
				</div>
			</div>
		</form>
	</div>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDc_fr5aLrsDzr9ZVYR9lpwD9TaaOoqKU&libraries=places&callback=initMap"
        async defer></script>
<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('#fecha_inauguracion').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' });
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $(".imagen").on('click', function(){
    	var modal = $("#modal-primary");
    	$(".modal-title").text( $(this).attr('img-name') );
    	$(".modal-body").html(`<img src="${ $(this).attr('imagen') }" class="img-thumbnail">`);
    	modal.modal({open: true});

    });

    $('.timepicker').timepicker({
      showInputs: false
    })
    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
              console.log('Checkbox clicked! New state=' + this.checked);
              autocomplete.setOptions({strictBounds: this.checked});
            });
      }
</script>


@endsection