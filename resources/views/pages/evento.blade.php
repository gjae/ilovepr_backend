@extends('layouts.dashboard_app_control')
@section('title', 'Eventos registrados')
@section('title_for_wrapper', 'Administracion de eventos')
@section('panel_header', 'Crear un evento pendientes')
@section('_css')

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}">
   <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('dash_content')
	<div class="container">
		<form action="{{ url($url) }}" method="POST" enctype="multipart/form-data">
			@csrf
			@if (!is_bool($evento))
				@method('PUT')
			@endif
			<div class="form-row">
				
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label for="">Nombre del evento</label>
					<input type="text" value="{{ is_bool($evento) ? '': $evento->nombre }}" placeholder="Nombre del evento" class="form-control" name="nombre" required="" id="nombre">
				</div>
				<div class="col-sm-4 col-lg-4 col-md-4">
					<label for="">Ciudad</label>
					<select name="ciudad_id" id="" class="form-control select2" required="" data-placeholder="Ciudad del evento">
						@foreach (App\Ciudad::all() as $ciudad)
							<option {{ !is_bool($evento) && $evento->ciudad_id == $ciudad->id ? 'selected': '' }} value="{{ $ciudad->id }}">
								{{ $ciudad->nombre }}
							</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-2 col-lg-2 col-md-2">
	              <div class="form-group">
	                <label>Fecha</label>

	                <div class="input-group">
	                  <div class="input-group-addon">
	                    <i class="fa fa-calendar"></i>
	                  </div>
	                  <input type="text" value="{{ is_bool($evento) ? '': $evento->fecha_hora->format('Y-m-d') }}" id="fecha" name="fecha" class="form-control" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask>
	                </div>
	                <!-- /.input group -->
	              </div>
				</div>
				<div class="col sm-2 col-lg-2 col-md-2">
					<label for="">Fecha del evento</label>
	                  <div class="input-group">
	                    <input type="text" value="{{ is_bool($evento) ? '': $evento->fecha_hora->format('H:i:s A') }}"  name="hora" class="form-control timepicker">

	                    <div class="input-group-addon">
	                      <i class="fa fa-clock-o"></i>
	                    </div>
	                  </div>
				</div>
			</div>
			<div class="form-row">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Categoria del evento</label>
					<select name="categoria_id" id="categoria_id" class="form-control select2" required  data-placeholder="Seleccione una categoría">
						<option value="">-- SELECCIONE UNA CATEGORIA --</option>
						@foreach (App\Categoria::all() as $categoria)
							<option {{ !is_bool($evento) && $evento->categoria_id == $categoria->id ? 'selected': '' }} value="{{ $categoria->id }}">
								{{ $categoria->nombre }}
							</option>
						@endforeach
					</select>
				</div>

				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Costo de la entrada</label>
					<input type="text" value="{{ is_bool($evento) ? '': $evento->costo_entrada }}"   value="0.00" class="form-control" name="costo_entrada" id="costo_entrada">
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Tipo de evento</label>
					<select name="tipo" id="tipo" class="form-control select2" required="" data-placeholder="Tipo de evento">
						<option {{ !is_bool($evento) && $evento->tipo == 'PUBLICO' ? 'selected': '' }} value="PUBLICO">Evento Publico</option>
						<option {{ !is_bool($evento) && $evento->tipo == 'PRIVADO' ? 'selected': '' }} value="PRIVADO">Evento Privado</option>
					</select>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Tipo de espacio</label>
					<select name="tipo_espacio" id="" class="form-control select2" required="" data-placeholder="Aire libre o espacio cerrado">
						<option {{ !is_bool($evento) && $evento->tipo_espacio == 'AIRE_LIBRE' ? 'selected': '' }} value="AIRE_LIBRE">Espacio al aire libre</option>
						<option {{ !is_bool($evento) && $evento->tipo_espacio == 'CERRADO' ? 'selected': '' }} value="CERRADO">
							Espacio cerrado
						</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<label for="">Descripción completa del evento</label>
					<textarea name="descripcion" id="descripcion" cols="30" required rows="3" class="form-control">{{ is_bool($evento) ? '': $evento->descripcion }}</textarea>

				</div>
			</div>
			<div class="form-row">
				<div class="col-sm-12 col-md-12 col-lg-12">
				<label for="">Imagenes promocionales</label>
				@if (!is_bool( $evento ))
					<table class="table table-bordered table-striped datatables">
						<thead>
							<tr>
								<th>Nombre original</th>
								<th>Tamaño</th>
								<th>Extension</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($evento->imagenes as $imagen)
								<tr>
									<td>{{ $imagen->imagen->nombre_original }}</td>
									<td>{{ $imagen->imagen->size }}</td>
									<td>{{ $imagen->imagen->original_extension }}</td>
									<td>
										<a class="btn btn-small btn-success imagen" imagen="{{ url($imagen->imagen->ubicacion) }}" img-name="{{ $imagen->imagen->nombre_original }}">
											<i class="fa fa-eye"></i>
										</a>
										<a class="btn btn-small btn-danger delete" type="dashboard/eventos/imagenes" data-delete="{{ $imagen->id }}">
											<i class="fa fa-remove"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@endif
				<input type="file" multiple class="form-control" name="images[]" id="imagens">
				</div>
			</div>
			<br><br>
			<hr>
			<br>
			<div class="form-row">
				<div class="col-sm-8 col-lg-8 col-md-8">
					
					<input type="submit" value="Guardar" class="btn btn-success">

				</div>
			</div>
		</form>
	</div>
  <div class="modal modal fade" id="modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
      </div>
            <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
  </div>
@endsection
@section('jquery')

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(document).ready( function(){
    $('.select2').select2()
    $('#fecha').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' });
    $('.actions').on('click', function(){
      var modal = $("#modal-primary");
      var url = location.protocol+'//'+location.host+`/${$(this).attr('type')}/${ $(this).attr('action') }`
      $.getJSON(url, {}, function(resp){
        if( resp.error ) modal = null;
        $(".modal-body").html( resp.html );
        $('.modal-title').text(resp.modal_title)
        modal.modal({open: true})
      });
    });

    $(".imagen").on('click', function(){
    	var modal = $("#modal-primary");
    	$(".modal-title").text( $(this).attr('img-name') );
    	$(".modal-body").html(`<img src="${ $(this).attr('imagen') }" class="img-thumbnail">`);
    	modal.modal({open: true});

    });

    $('.timepicker').timepicker({
      showInputs: false
    })
    $('.delete').on('click', function(){

      if( confirm('¿seguro que desea ejecutar esta acción? No podra ser revertida.') )
      {
        var url = location.protocol+'//'+location.host+`/${ $(this).attr('type')}/${ $(this).attr('data-delete') }`;
        $.post(url, { _method : 'DELETE', _token: "{{ csrf_token() }}" }, function(resp){
          if( confirm( resp.message ) ) {
          	location.reload()
          }
        });
      }
    });
    $('.datatables').DataTable();
  });
</script>

@endsection