<form action="{{ url($url) }}" method="POST">
	
	@csrf

	@if (!is_bool($categoria))
		@method('PUT')
	@endif
	<div class="form-row">
		<div class="col-sm-10 col-lg-10 col-md-10">
			<label for="">Nombre de la categoria</label>
			<input type="text" value="{{ is_bool($categoria) ? '' : $categoria->nombre }}"  placeholder="Categoria" class="form-control" name="nombre" required="" id="nombre">
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">ID #</label>
			<input type="text" value="{{ is_bool($categoria)  ? App\Categoria::withTrashed()->count() + 1 : $categoria->id }}" class="form-control" disabled="">
		</div>
	</div>
	
	<br><br>
	<hr>

		
	<div class="form-row">
		@include('partials.footer_modals')
	</div>
</form>