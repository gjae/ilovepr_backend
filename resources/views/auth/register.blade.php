@extends('layouts.auth_layout')
@section('title', 'Registar Una Cadena')
@section('content')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Pro</b>Hotel</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Registrar nueva cadena</p>

    <form action="{{ route('register') }}" method="post">
      @csrf
      <input type="hidden" name="rol" value="CDA_HOTEL ">
      <div class="form-group has-feedback">
          <input type="text" placeholder="Nombre de la cadena hotelera" class="form-control{{ $errors->has('cadena') ? ' is-invalid' : '' }}" name="cadena">
           <span class="fa fa-hotel form-control-feedback"></span>
      </div>
      @include('partials.register_form')
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Aceptar <a href="#">terminos y condiciones</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

   <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> -->

    <a href="{{ route('login') }}" class="text-center">Mi cadena ya esta registrada</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

@endsection