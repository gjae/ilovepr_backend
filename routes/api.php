<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth', 'api\v1\Authentication@login');

Route::group([ 'middleware' => 'auth:api', 'namespace' => '\App\Http\Controllers\api\v1' ], function(){
    Route::get('me' , 'Authentication@me');
    Route::get('logout', 'Authentication@logout');
    Route::post('places/save', 'PlacesController@save');
    Route::get('places', 'PlacesController@places');
    Route::get('places/get', 'PlacesController@getAll');
    Route::get('/ciudades', 'PlacesController@ciudades');
    Route::group(['prefix' => 'hotels'], function(){
        Route::get('/', 'HotelsController@get');
        Route::post('/comment', 'PlacesController@comment');
    });

    Route::group(['prefix' => 'events'], function(){
        Route::get('/', 'EventsController@get');
        Route::post('/save', 'EventsController@save');
    });
});


Route::get('myUser', function(){
    return response(['error' => false, 'message' => 'recibido'], 200)->header('Content-Type', 'application/json');
})->middleware('auth.basic.once');
