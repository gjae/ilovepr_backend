<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return  redirect()->to( url('login') );
});


Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function(){
	Route::resource('/', 'DashboardController');
	Route::resource('/eventos', 'EventosController');
	Route::resource('/lugares', 'LugaresController');
	Route::delete('/eventos/imagenes/{img_id}', "EventosController@imagen");
	Route::delete('/lugares/imagenes/{img_id}', "LugaresController@imagen");
	Route::resource('/categorias', 'CategoriaController');

	Route::get('/logout', function(){
		Auth::logout();
		return redirect()->to( url("dashboard") );
	});
});

Route::get('/home', 'HomeController@index')->name('home');
